/* global Voronoi, document, window, setTimeout, clearTimeout, Perlin */
/* exported init, clearSites */
/* jshint curly: false */

var voronoi = new Voronoi();
var diagram = null;
var margin = 0.01;
var canvas = null;
var bbox = {
    xl: 0,
    xr: 0,
    yt: 0,
    yb: 0
};
var sites = [];
var cells = [];
var numberOfSites = 1000;
var numberOfRelaxations = 10;
var timeout = null;
var frameFrequency = 1 / 30;
var complete = false;
var edgesPerFrame = 5;
var edgesToDraw = [];
var edgeQueue = [];
var visitedEdges = {};
var animated = false;
var perlin = null;

function init() {
    canvas = document.getElementById('voronoi');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    bbox.xr = window.innerWidth;
    bbox.yb = window.innerHeight;
    randomSites(numberOfSites, true);

    if (animated) {
        setupRender();
        timeout = setTimeout(animatedRender, frameFrequency);
    }

    createIsland();
    render();
}

function clearSites() {
    compute([]);
}

function randomSites(n, clear) {
    var i = 0;
    var sites = [];
    if (!clear)
        sites = sites.slice(0);

    // create vertices
    var xmargin = canvas.width * margin,
        ymargin = canvas.height * margin,
        xo = xmargin,
        dx = canvas.width - xmargin * 2,
        yo = ymargin,
        dy = canvas.height - ymargin * 2;

    for (i = 0; i < n; i++) {
        sites.push({
            x: Math.round((xo + Math.random() * dx) * 10) / 10,
            y: Math.round((yo + Math.random() * dy) * 10) / 10
        });
    }

    compute(sites);

    for (i = 0; i < numberOfRelaxations; i++)
        relaxSites();

    cells = diagram.cells;
}

function relaxSites() {
    if (!diagram)
        return;

    var tempCells = diagram.cells,
        iCell = tempCells.length,
        cell,
        site, newsites = [],
        again = false,
        rn, dist;
    var p = 1 / iCell * 0.1;

    while (iCell--) {
        cell = tempCells[iCell];
        rn = Math.random();
        // probability of apoptosis
        if (rn < p) {
            continue;
        }
        site = cellCentroid(cell);
        dist = distance(site, cell.site);
        again = again || dist > 1;
        // don't relax too fast
        if (dist > 2) {
            site.x = (site.x + cell.site.x) / 2;
            site.y = (site.y + cell.site.y) / 2;
        }
        // probability of mytosis
        if (rn > (1 - p)) {
            dist /= 2;
            newsites.push({
                x: site.x + (site.x - cell.site.x) / dist,
                y: site.y + (site.y - cell.site.y) / dist,
            });
        }
        newsites.push(site);
    }

    compute(newsites);
}

function distance(a, b) {
    var dx = a.x - b.x,
        dy = a.y - b.y;

    return Math.sqrt(dx * dx + dy * dy);
}

function cellArea(cell) {
    var area = 0,
        halfedges = cell.halfedges,
        iHalfedge = halfedges.length,
        halfedge,
        p1, p2;

    while (iHalfedge--) {
        halfedge = halfedges[iHalfedge];
        p1 = halfedge.getStartpoint();
        p2 = halfedge.getEndpoint();
        area += p1.x * p2.y;
        area -= p1.y * p2.x;
    }

    area /= 2;
    return area;
}

function cellCentroid(cell) {
    var x = 0,
        y = 0,
        halfedges = cell.halfedges,
        iHalfedge = halfedges.length,
        halfedge,
        v, p1, p2;

    while (iHalfedge--) {
        halfedge = halfedges[iHalfedge];
        p1 = halfedge.getStartpoint();
        p2 = halfedge.getEndpoint();
        v = p1.x * p2.y - p2.x * p1.y;
        x += (p1.x + p2.x) * v;
        y += (p1.y + p2.y) * v;
    }
    v = cellArea(cell) * 6;

    return {
        x: x / v,
        y: y / v
    };
}

function compute(newsites) {
    sites = newsites;
    voronoi.recycle(diagram);
    diagram = voronoi.compute(sites, bbox);
}

function recurse(edge) {
    if (edge && !visitedEdges[edge.id]) {
        visitedEdges[edge.id] = true;
        edgeQueue.push(edge);
        var e = 0;

        if (edge.va && edge.va.edges.length > 1) {
            for (e = 0; e < edge.va.edges.length; e++)
                recurse(edge.va.edges[e]);
        }

        if (edge.vb && edge.vb.edges.length > 1) {
            for (e = 0; e < edge.vb.edges.length; e++)
                recurse(edge.va.edges[e]);
        }
    }
}

function setupRender() {
    var edges = diagram.edges;
    recurse(edges[0]);
}

function animatedRender() {
    if (!diagram)
        return;

    var ctx = canvas.getContext('2d');
    ctx.canvas.width = window.innerWidth;
    ctx.canvas.height = window.innerHeight;

    // background
    ctx.globalAlpha = 1;
    ctx.beginPath();
    ctx.rect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = 'white';
    ctx.fill();
    ctx.strokeStyle = '#888';
    ctx.stroke();

    // edges
    ctx.beginPath();
    ctx.strokeStyle = '#222';
    var edge = null;
    var v = null;
    var i = 0;

    for (; i < edgesPerFrame; i++) {
        var popped = edgeQueue.shift();
        if (popped)
            edgesToDraw.push(popped);
        else
            complete = true;
    }

    for (i = 0; i < edgesToDraw.length; i++) {
        edge = edgesToDraw[i];
        v = edge.va;
        ctx.moveTo(v.x, v.y);
        v = edge.vb;
        ctx.lineTo(v.x, v.y);
    }

    ctx.stroke();

    // sites
    ctx.beginPath();
    ctx.fillStyle = '#FF0000';

    var vertices = diagram.vertices;
    //var newsites = sites,
    var iSite = vertices.length;

    while (iSite--) {
        v = vertices[iSite];
        if (v.edges.length > 2)
            ctx.rect(v.x - 2 / 3, v.y - 2 / 3, 2, 2);
    }

    ctx.fill();

    if (complete) {
        clearTimeout(timeout);
        edgesToDraw = null;
        edgeQueue = null;
        visitedEdges = null;
    } else
        timeout = setTimeout(animatedRender, frameFrequency);
}

function isLand(cell) {
    if (!cell || !cell.site || !perlin)
        return false;

    var noiseAmount = perlin.noise(cell.site.x / 750, cell.site.y / 750, 0);
    return noiseAmount < 0.3;
}

function createIsland() {
    perlin = new Perlin(new Date().toString());

    for (var i = 0; i < cells.length; i++) {
        cells[i].Water = !isLand(cells[i]);
    }
}

function render() {
    if (!diagram)
        return;

    var ctx = canvas.getContext('2d');
    ctx.canvas.width = window.innerWidth;
    ctx.canvas.height = window.innerHeight;

    // background
    ctx.globalAlpha = 1;
    ctx.beginPath();
    ctx.rect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = 'white';
    ctx.fill();
    ctx.strokeStyle = '#888';
    ctx.stroke();

    // sites

    var v = null;
    var iSite = sites.length;

    while (iSite--) {
        v = cells[iSite];
        var halfEdges = v.halfedges;
        var lastPoint = null;

        if (v && halfEdges) {
            ctx.beginPath();
            ctx.fillStyle = v.Water ? '#0000FF' : '#00FF00';

            for (var i = 0; i < halfEdges.length; i++) {
                var edge = halfEdges[i].edge;
                if (i === 0) {
                    ctx.moveTo(edge.va.x, edge.va.y);
                    ctx.lineTo(edge.vb.x, edge.vb.y);
                    lastPoint = edge.vb;
                } else {
                    if (lastPoint.x == edge.va.x && lastPoint.y == edge.va.y) {
                        ctx.lineTo(edge.vb.x, edge.vb.y);
                        lastPoint = edge.vb;
                    } else if (lastPoint.x == edge.vb.x && lastPoint.y == edge.vb.y) {
                        ctx.lineTo(edge.va.x, edge.va.y);
                        lastPoint = edge.va;
                    } else {
                        //debugger;
                    }
                }
            }

            ctx.closePath();
            ctx.fill();
        }
    }
}
